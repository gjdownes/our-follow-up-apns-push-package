<?php

$certificate_path = "certs/cert.p12";
$intermediate_certificate_path = "certs/intermediate_cert.pem"; 
$certificate_password = "G_j_D_07"; 
$website_push_id = "web.dev.practicebetter.messaging";

$DOMAIN = "ngrok.io";
$web_service_url = "https://practicebetter.$DOMAIN/hooks/apns";
$domain_template = "https://%s.$DOMAIN";

function create_website_json($package_dir, $primary_subdomain, $allowed_domains, $user_id) {
	global $website_push_id;
	global $web_service_url;
	global $DOMAIN;
	
	$properties = array();
	$properties["websiteName"] = "Practice Better";
	$properties["websitePushID"] = $website_push_id;
	$properties["allowedDomains"] = $allowed_domains;
	$properties["urlFormatString"] = "https://$primary_subdomain.$DOMAIN/#/redirect?ht=%@";
	$properties["authenticationToken"] = $user_id;
	$properties["webServiceURL"] = $web_service_url;
	
	$file_path = "$package_dir/website.json";
	
	file_put_contents($file_path, json_encode((object)$properties, JSON_UNESCAPED_SLASHES));
}

// Convenience function that returns an array of raw files needed to construct the package.
function raw_files() {
    return array(
        'icon.iconset/icon_16x16.png',
        'icon.iconset/icon_16x16@2x.png',
        'icon.iconset/icon_32x32.png',
        'icon.iconset/icon_32x32@2x.png',
        'icon.iconset/icon_128x128.png',
        'icon.iconset/icon_128x128@2x.png',
    );
}

// Copies the raw push package files to $package_dir.
function copy_raw_push_package_files($package_dir) {
    mkdir($package_dir . '/icon.iconset');
    foreach (raw_files() as $raw_file) {
        copy("pushPackage.raw/$raw_file", "$package_dir/$raw_file");
    }
}

// Creates the manifest by calculating the SHA1 hashes for all of the raw files in the package.
function create_manifest($package_dir) {
    // Obtain SHA1 hashes of all the files in the push package
    $manifest_data = array();
    foreach (raw_files() as $raw_file) {
        $manifest_data[$raw_file] = sha1(file_get_contents("$package_dir/$raw_file"));
    }
	$manifest_data['website.json'] = sha1(file_get_contents("$package_dir/website.json"));
    file_put_contents("$package_dir/manifest.json", json_encode((object)$manifest_data, JSON_UNESCAPED_SLASHES));
}

// Creates a signature of the manifest using the push notification certificate.
function create_signature($package_dir, $cert_path, $cert_password, $intermediate_certificate_path) {
    // Load the push notification certificate
    $pkcs12 = file_get_contents($cert_path);
    $certs = array();
    if(!openssl_pkcs12_read($pkcs12, $certs, $cert_password)) {
        return;
    }

    $signature_path = "$package_dir/signature";

    // Sign the manifest.json file with the private key from the certificate
    $cert_data = openssl_x509_read($certs['cert']);
    $private_key = openssl_pkey_get_private($certs['pkey'], $cert_password);
    openssl_pkcs7_sign("$package_dir/manifest.json", $signature_path, $cert_data, $private_key, array(), PKCS7_BINARY | PKCS7_DETACHED, $intermediate_certificate_path);

    // Convert the signature from PEM to DER
    $signature_pem = file_get_contents($signature_path);
    $matches = array();
    if (!preg_match('~Content-Disposition:[^\n]+\s*?([A-Za-z0-9+=/\r\n]+)\s*?-----~', $signature_pem, $matches)) {
        return;
    }
    $signature_der = base64_decode($matches[1]);
    file_put_contents($signature_path, $signature_der);
}

// Zips the directory structure into a push package, and returns the path to the archive.
function package_raw_data($package_dir) {
    $zip_path = "$package_dir.zip";

    // Package files as a zip file
    $zip = new ZipArchive();
    if (!$zip->open("$package_dir.zip", ZIPARCHIVE::CREATE)) {
        error_log('Could not create ' . $zip_path);
        return;
    }

    $raw_files = raw_files();
    $raw_files[] = 'website.json';
    $raw_files[] = 'manifest.json';
    $raw_files[] = 'signature';
    foreach ($raw_files as $raw_file) {
        $zip->addFile("$package_dir/$raw_file", $raw_file);
    }

    $zip->close();
}

// Creates the push package, and returns the path to the archive.
function create_push_package($package_dir) {
    global $certificate_path, $certificate_password, $intermediate_certificate_path;

    copy_raw_push_package_files($package_dir);
    create_manifest($package_dir);
    create_signature($package_dir, $certificate_path, $certificate_password, $intermediate_certificate_path);
    $package_path = package_raw_data($package_dir);
	delete_directory($package_dir);

    return $package_path;
}

function delete_directory($dir) {
    if (!file_exists($dir)) {
        return true;
    }

    if (!is_dir($dir)) {
        return unlink($dir);
    }

    foreach (scandir($dir) as $item) {
        if ($item == '.' || $item == '..') {
            continue;
        }

        if (!delete_directory($dir . DIRECTORY_SEPARATOR . $item)) {
            return false;
        }
    }

    return rmdir($dir);
}

function get_url_param($name) {
	return htmlspecialchars($_GET[$name]);
}

function get_allowed_domains($name) {
	global $domain_template;

	$raw_param = htmlspecialchars($_GET[$name]);
	$subdomains = explode(",", $raw_param);
	
	$allowed_domains = array();
	foreach($subdomains as $subdomain) {
		array_push($allowed_domains, sprintf($domain_template, $subdomain)); 
	}
	
	return $allowed_domains;
}

function send_response($package_path) {
	if (empty($package_path)) {
		http_response_code(500);
		die;
	}

	header("Content-type: application/zip");
	echo file_get_contents($package_path);
}

// MAIN
$primary_subdomain = get_url_param("primary");
$allowed_domains = get_allowed_domains("domains");
$user_id = get_url_param("userid");
$package_dir = "pushPackage.$user_id.$primary_subdomain";
$package_path = "pushPackage.$user_id.$primary_subdomain.zip";

if (file_exists($package_path)) {
	send_response($package_path);
	die;
}

// Create a temporary directory in which to assemble the push package   
if (file_exists($package_dir)) {
	delete_directory($package_dir);
}

mkdir($package_dir, 0777, true);
create_website_json($package_dir, $primary_subdomain, $allowed_domains, $user_id);
create_push_package($package_dir);
send_response($package_path);
//unlink($package_path);
die;